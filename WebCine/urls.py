"""WebCine URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

#token api
#from rest_framework.authtoken import views
#from Cine.views import Login, Logout

from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    path('', include('Cine.urls')),
    path('admin/', admin.site.urls, name='admin'),
    path('accounts/', include('registration.backends.default.urls')),
    path('select2/', include('django_select2.urls')),
    path('api/', include(('Cine.urls', 'api'))),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth')


    #path('api_generate_token/', views.obtain_auth_token),
    #path('login/', Login.as_view(), name='login'),
    #path('logout/', Logout.as_view(), name='logout'),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#personalizacion de los titulos del adminsitrador de django
admin.site.site_header = "Administracion de Cineastas"
admin.site.index_title = "Cineastas"
admin.site.site_title = "Administracion Cineastas"
