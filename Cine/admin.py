from django.contrib import admin
from .models import Idioma, Pelicula, Cine, Funcion, Genero, Comment, Denuncia, Megusta

# Register your models here.

class PeliculaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'imagen', 'duracionMinutos', 'generos', 'idiomas')
    #permite buscar por marca
    search_fields = ['nombre']
    #permite filtrar por genero
    list_filter = ('genero',)

    #se hace las siguintes funciones ya q genero y idioma tiene formato ManyToManyField
    def generos(self, obj):
        return "\n".join([p.genero for p in obj.genero.all()])
    def idiomas(self, obj):
        return "\n".join([p.idioma for p in obj.idioma.all()])


class CineAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'direccion', 'horariosInicio', 'horariosFin', 'estado')


class FuncionAdmin(admin.ModelAdmin):
    list_display = ('cine', 'pelicula', 'dia', 'horario', 'idioma', 'subtitulo', 'estreno')
    search_fields = ['pelicula']
    list_filter = ('pelicula', 'cine', 'dia',)


class CommentAdmin(admin.ModelAdmin):
    list_display = ('text', 'pelicula', 'usuario', 'reply', 'created_date', 'approved_comment', 'cantidad_denuncia')


class DenunciaAdmin(admin.ModelAdmin):
    list_display = ('usuario', 'comment', 'motivo')
    list_filter = ('comment',)


class MegustaAdmin(admin.ModelAdmin):
    list_display = ('usuario', 'pelicula')


admin.site.register(Idioma)
admin.site.register(Genero)
admin.site.register(Megusta, MegustaAdmin)
admin.site.register(Denuncia, DenunciaAdmin)
admin.site.register(Pelicula, PeliculaAdmin)
admin.site.register(Cine, CineAdmin)
admin.site.register(Funcion, FuncionAdmin)
admin.site.register(Comment, CommentAdmin)
