from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


# Create your models here.
class Genero(models.Model):
    genero = models.CharField(max_length=50)
    def __str__(self):
        return self.genero


class Idioma(models.Model):
    idioma = models.CharField(max_length=50)
    def __str__(self):
        return self.idioma


class Pelicula(models.Model):
    nombre = models.CharField(max_length=50)
    imagen = models.ImageField(upload_to="pelicula", blank=True, null=True)
    duracionMinutos = models.IntegerField(default=0, blank=True, null=True)
    genero = models.ManyToManyField(Genero)
    idioma = models.ManyToManyField(Idioma)

    def __str__(self):
        return self.nombre


class Cine(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name='cines_propios')
    nombre = models.CharField(max_length=50)
    imagen = models.ImageField(upload_to="cine", blank=True, null=True)
    direccion = models.CharField(max_length=50)
    horariosInicio = models.TimeField()
    horariosFin = models.TimeField()
    estado = models.BooleanField(default=False)
    #usuariosadm = models.ManyToManyField(User, related_name='cines_administrados',blank=True, null=True)

    def __str__(self):
        return self.nombre


class Funcion(models.Model):
    cine = models.ForeignKey(Cine, on_delete=models.CASCADE)
    pelicula = models.ForeignKey(Pelicula, on_delete=models.CASCADE)
    estreno = models.BooleanField(default=True)
    dia = models.DateField(blank=True, null=True)
    horario = models.TimeField(blank=True, null=True)
    idioma = models.ForeignKey(Idioma, on_delete=models.CASCADE)
    subtitulo = models.BooleanField(default=False)
    votos = models.ManyToManyField(User, related_name='votos', blank=True)

    class Meta:
        unique_together = ('cine', 'pelicula', 'dia', 'horario', 'idioma', 'subtitulo')

    def total_votos(self):
        return self.votos.count()


class Comment(models.Model):
    pelicula = models.ForeignKey(Pelicula, on_delete=models.CASCADE, blank=True, null=True, related_name='comments')
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    funcion = models.ForeignKey(Funcion, on_delete=models.CASCADE, blank=True, null=True)
    reply = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True, related_name='replies')
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=True)
    cantidad_denuncia = models.IntegerField(default=0, blank=True, null=True)

    def __str__(self):
        return self.text


class Denuncia(models.Model):
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    motivo = models.TextField()

    class Meta:
        unique_together = ('comment', 'usuario')

    def __str__(self):
        return self.motivo


class Megusta(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    pelicula = models.ForeignKey(Pelicula, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        unique_together = ('usuario', 'pelicula')

