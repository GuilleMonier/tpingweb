from builtins import super
from django import forms
from .models import Cine, Pelicula, Genero, Idioma, Funcion, Comment, Denuncia
#from django.contrib.admin import widgets
from django_select2.forms import Select2Widget


class PeliculasForm(forms.ModelForm):
    class Meta:
        model = Pelicula
        fields = ['nombre', 'imagen', 'duracionMinutos', 'genero', 'idioma']
        labels = {
            'duracionMinutos': 'Duracion en minutos',
        }



class CineForm(forms.ModelForm):
    class Meta:
        model = Cine
        fields = ['nombre', 'imagen', 'direccion', 'horariosInicio', 'horariosFin']
        widgets = {
            'usuario': forms.HiddenInput(),
            'horariosInicio': forms.TextInput(attrs={'class': 'form-control', 'type': 'time'}),
            'horariosFin': forms.TextInput(attrs={'class': 'form-control', 'type': 'time'}),
        }
        labels = {
            'horariosInicio': 'Horario Inicio',
            'horariosFin': 'Horario Fin',
        }


class GeneroForm(forms.ModelForm):
    class Meta:
        model = Genero
        fields = ['genero']


class IdiomaForm(forms.ModelForm):
    class Meta:
        model = Idioma
        fields = ['idioma']


class FuncionForm(forms.ModelForm):
    pelicula= forms.ModelChoiceField(queryset=Pelicula.objects.all(), widget=Select2Widget)
    def __init__(self, user, *args, **kwargs):
        super(FuncionForm, self).__init__(*args, **kwargs)
        self.fields['cine'].queryset = Cine.objects.filter(usuario=user)

    class Meta:

        model = Funcion
        fields = ['cine', 'pelicula', 'dia', 'horario', 'idioma', 'subtitulo', 'estreno']
        widgets = {
            'dia': forms.TextInput(attrs={'class': 'form-control', 'type': 'date'}),
            'horario': forms.TextInput(attrs={'class': 'form-control', 'type': 'time'}),
        }


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Ingrese comentario', 'rows': '4', 'cols': '50'}),
        }


class DenunciaForm(forms.ModelForm):
    class Meta:
        model = Denuncia
        fields = ['motivo']
        widgets = {
            'motivo': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': 'Ingrese motivo', 'rows': '4', 'cols': '50'}),
        }