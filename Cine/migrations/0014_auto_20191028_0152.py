# Generated by Django 2.2.4 on 2019-10-28 01:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Cine', '0013_auto_20191027_2300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='denuncia',
            name='comment',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='Cine.Comment'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='denuncia',
            name='usuario',
            field=models.ForeignKey(default=54, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
