# Generated by Django 2.2.4 on 2019-11-04 04:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Cine', '0019_auto_20191103_1811'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pelicula',
            name='duracion',
        ),
        migrations.AddField(
            model_name='pelicula',
            name='duracionMinutos',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
    ]
