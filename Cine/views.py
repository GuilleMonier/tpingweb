from datetime import date
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Cine, Pelicula, Funcion, Genero, Idioma, User, Comment, Denuncia, Megusta
from .forms import PeliculasForm, CineForm, GeneroForm, IdiomaForm, FuncionForm, CommentForm, DenunciaForm
from django.contrib.auth.decorators import permission_required
from django.contrib.admin.views.decorators import staff_member_required
from django.core.paginator import Paginator
from django.template.loader import render_to_string
from django.http import JsonResponse
#serialzar rest
from rest_framework import viewsets, generics
from .serializers import GeneroSerializer, IdiomaSerializer, PeliculaSerializer
#para token
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.generic.edit import FormView
from django.contrib.auth import login, logout, authenticate
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import AuthenticationForm
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from builtins import super


class GeneroList(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Genero.objects.all()
    serializer_class = GeneroSerializer
    # permission_classes = (IsAuthenticated,)
    # authentication_class = (TokenAuthentication,)


class GeneroDetail(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Genero.objects.all()
    serializer_class = GeneroSerializer


class IdiomaList(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Idioma.objects.all()
    serializer_class = IdiomaSerializer
    # permission_classes = (IsAuthenticated,)
    # authentication_class = (TokenAuthentication,)


class IdiomaDetail(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Idioma.objects.all()
    serializer_class = IdiomaSerializer


class PeliculaList(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Pelicula.objects.all()
    serializer_class = PeliculaSerializer
    # permission_classes = (IsAuthenticated,)
    # authentication_class = (TokenAuthentication,)


class PeliculaDetail(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Pelicula.objects.all()
    serializer_class = PeliculaSerializer


# class Login(FormView):
#     template_name = "login.html"
#     form_class = AuthenticationForm
#     success_url = reverse_lazy('api:genero_list')
#
#     @method_decorator(csrf_protect)
#     @method_decorator(never_cache)
#     def dispatch(self, request, *args, **kwargs):
#         if request.user.is_authenticated:
#             return HttpResponseRedirect(self.get_succes_url())
#         else:
#             return super(Login, self).dispatch(request, *args, **kwargs)
#
#     def form_valid(self, form):
#         user = authenticate(username=form.cleaned_data['username'],
#                             password=form.cleaned_data['password'])
#         token,_ = Token.objects.get_or_create(user=user)
#         if token:
#             login(self.request, form.get_user())
#             return super(Login, self).form_valid(form)
#
#
# class Logout(APIView):
#     def get(self, request, format=None):
#         request.user.auth_token.delete()
#         logout(request)
#         return Response(status=status.HTTP_200_OK)


def Home(request):
    cines = Cine.objects.filter(estado="True")
    peliculas_lista = Pelicula.objects.all()

    paginator_pelicula = Paginator(peliculas_lista, 6)
    page_pelicula = request.GET.get('page')
    peliculas = paginator_pelicula.get_page(page_pelicula)

    return render(request, 'home.html', {'cines': cines, 'peliculas': peliculas})


def About(request):
    return render(request, "about.html", {})


@login_required
def Administrar(request):
    return render(request, "administrar.html", {})


@login_required
def VotacionEstreno(request):
    return render(request, 'votacionestreno.html', {})


def MostrarCine(request, id):
    cine = Cine.objects.get(id=id)
    return render(request, 'mostrarcine.html', {'cine': cine})


def MostrarFunciones(request, id):
    cine = Cine.objects.get(id=id)
    listaFuncionesEstrenos = Funcion.objects.filter(cine=cine, estreno="True", dia__gt=date.today()).order_by('dia', 'horario')
    listaFunciones = Funcion.objects.filter(cine=cine, estreno="False", dia__gt=date.today()).order_by('dia', 'horario')
    listaTodas = Funcion.objects.filter(cine=cine, dia__gt=date.today()).order_by('dia', 'horario')

    paginator_pelicula = Paginator(listaTodas, 3)
    page_pelicula = request.GET.get('page')
    listaToda = paginator_pelicula.get_page(page_pelicula)

    paginator_pelicula = Paginator(listaFuncionesEstrenos, 3)
    page_pelicula = request.GET.get('page')
    listaFuncionesEstreno = paginator_pelicula.get_page(page_pelicula)

    paginator_pelicula = Paginator(listaFunciones, 3)
    page_pelicula = request.GET.get('page')
    listaFunciones = paginator_pelicula.get_page(page_pelicula)

    return render(request, 'mostrarfunciones.html', {'listaFunciones': listaFunciones,
                                                     'listaFuncionesEstreno': listaFuncionesEstreno,
                                                     'listaToda': listaToda,
                                                     'cine': cine})


def MostrarPeliculas(request, id):
    cine = Cine.objects.get(id=id)

    #funcionEstreno = Funcion.objects.filter(cine=cine, estreno="True", dia__gt=date.today())
    peliculaEstrenos= Pelicula.objects.filter(id__in=Funcion.objects.filter(cine=cine, estreno="True", dia__gt=date.today()).values("pelicula_id"))

    #funcionpelicula = Funcion.objects.filter(cine=cine, estreno="False", dia__gt=date.today())
    peliculas = Pelicula.objects.filter(id__in=Funcion.objects.filter(cine=cine, estreno="False", dia__gt=date.today()).values("pelicula_id"))

    paginator_pelicula = Paginator(peliculaEstrenos, 6)
    page_pelicula = request.GET.get('page')
    listaEstreno = paginator_pelicula.get_page(page_pelicula)

    paginator_pelicula = Paginator(peliculas, 6)
    page_pelicula = request.GET.get('page')
    listapelicula = paginator_pelicula.get_page(page_pelicula)

    return render(request, 'mostrarpeliculas.html', {'listaEstreno': listaEstreno,
                                                     'listapelicula': listapelicula,
                                                     'cine': cine, })


########################################### ADMINISTRAR PELICULA#################################################


@staff_member_required
def AdministarPelicula(request):
    peliculas = Pelicula.objects.all()
    return render(request, 'administrarpelicula.html', {'peliculas': peliculas})


@staff_member_required
def CrearPelicula(request):
    if request.method == 'POST':
        pelicula_form = PeliculasForm(request.POST, request.FILES)
        if pelicula_form.is_valid():
            pelicula_form.save()
            return redirect('administrarpelicula')
    else:
        pelicula_form = PeliculasForm()
    return render(request, 'crearpelicula.html', {'pelicula_form': pelicula_form})


@staff_member_required
def EditarPelicula(request, id):
    pelicula = Pelicula.objects.get(id=id)
    if request.method == 'GET':
        pelicula_form = PeliculasForm(instance=pelicula)
    else:
        pelicula_form = PeliculasForm(request.POST, instance=pelicula)
        if pelicula_form.is_valid():
            pelicula_form.save()
        return redirect('administrarpelicula')
    return render(request, 'crearpelicula.html', {'pelicula_form': pelicula_form})


@staff_member_required
def EliminarPelicula(request, id):
    pelicula = Pelicula.objects.get(id=id)
    pelicula.delete()
    return redirect('administrarpelicula')


########################################### ADMINISTRAR GENERO#################################################
@staff_member_required
def AdministarGenero(request):
    genero = Genero.objects.all()
    return render(request, 'administrargenero.html', {'genero': genero})


@staff_member_required
def CrearGenero(request):
    if request.method == 'POST':
        genero_form = GeneroForm(request.POST)
        if genero_form.is_valid():
            genero_form.save()
            return redirect('administrargenero')
    else:
        genero_form = GeneroForm()
    return render(request, 'creargenero.html', {'genero_form': genero_form})


@staff_member_required
def EditarGenero(request, id):
    genero = Genero.objects.get(id=id)
    if request.method == 'GET':
        genero_form = GeneroForm(instance=genero)
    else:
        genero_form = GeneroForm(request.POST, instance=genero)
        if genero_form.is_valid():
            genero_form.save()
        return redirect('administrargenero')
    return render(request, 'creargenero.html', {'genero_form': genero_form})


@staff_member_required
def EliminarGenero(request, id):
    genero = Genero.objects.get(id=id)
    genero.delete()
    return redirect('administrargenero')


########################################### ADMINISTRAR IDIOMA#################################################

@staff_member_required
def AdministarIdioma(request):
    idioma = Idioma.objects.all()
    return render(request, 'administraridioma.html', {'idioma': idioma})


@staff_member_required
def CrearIdioma(request):
    if request.method == 'POST':
        idioma_form = IdiomaForm(request.POST);
        #idioma_form.idioma = request.POST.get('idioma')
        if idioma_form.is_valid():
            idioma_form.save()
            return redirect('administraridioma')
    else:
        idioma_form = IdiomaForm()
    return render(request, 'crearidioma.html', {'idioma_form': idioma_form})


@staff_member_required
def EditarIdioma(request, id):
    idioma = Idioma.objects.get(id=id)
    if request.method == 'GET':
        idioma_form = IdiomaForm(instance=idioma)
    else:
        idioma_form = IdiomaForm(request.POST, instance=idioma)
        if idioma_form.is_valid():
            idioma_form.save()
        return redirect('administraridioma')
    return render(request, 'crearidioma.html', {'idioma_form': idioma_form})


@staff_member_required
def EliminarIdioma(request, id):
    idioma = Idioma.objects.get(id=id)
    idioma.delete()
    return redirect('administraridioma')

########################################### ADMINISTRAR CINE#################################################


@login_required
def AdministarCine(request):
    cine = Cine.objects.filter(usuario=request.user)
    return render(request, 'administrarcine.html', {'cine': cine})


@login_required
def CrearCine(request):
    if request.method == 'POST':
        cine_form = CineForm(request.POST, request.FILES)
        print (cine_form)
        if cine_form.is_valid():
            nuevoCine = cine_form.save(commit=False)
            nuevoCine.usuario = request.user
            nuevoCine.save()
            return redirect('administrarcine')
    else:
        cine_form = CineForm()
    return render(request, 'crearcine.html', {'cine_form': cine_form})


@login_required
def EditarCine(request, id):
    cine = Cine.objects.get(id=id)
    if request.method == 'GET':
        cine_form = CineForm(instance=cine)
    else:
        cine_form = CineForm(request.POST, instance=cine)
        if cine_form.is_valid():
            cine_form.save()
        return redirect('administrarcine')
    return render(request, 'crearcine.html', {'cine_form': cine_form})


@login_required
def EliminarCine(request, id):
    cine = Cine.objects.get(id=id)
    cine.delete()
    return redirect('administrarcine')

########################################### ADMINISTRAR FUNCION#################################################


@login_required
def AdministarFuncion(request):
    funciones = Funcion.objects.filter(cine__usuario=request.user)
    return render(request, 'administrarfuncion.html', {'funciones': funciones})


@login_required
def CrearFuncion(request):
    if request.method == 'POST':
        funcion_form = FuncionForm(request.user, request.POST)
        if funcion_form.is_valid():
            funcion_form.save()
            return redirect('administrarfuncion')
    else:
        funcion_form = FuncionForm(request.user)
    return render(request, 'crearfuncion.html', {'funcion_form': funcion_form})


@login_required
def EditarFuncion(request, id):
    funcion = Funcion.objects.get(id=id)
    if request.method == 'GET':
        funcion_form = FuncionForm(request.user, instance=funcion)
    else:
        funcion_form = FuncionForm(request.user, request.POST, instance=funcion)
        if funcion_form.is_valid():
            funcion_form.save()
        return redirect('administrarfuncion')
    return render(request, 'crearfuncion.html', {'funcion_form': funcion_form})


@login_required
def EliminarFuncion(request, id):
    funcion = Funcion.objects.get(id=id)
    funcion.delete()
    return redirect('administrarfuncion')


def Detalle(request, id):
    pelicula = Pelicula.objects.get(id=id)
    #if request.user.is_authenticated:
        #denuncia = Denuncia.objects.filter(usuario=request.user)
        #denuncia = Comment.objects.filter(id__in=Denuncia.objects.filter(usuario=request.user).values("comment_id"))
    #else:
        #denuncia = None

    comments = Comment.objects.filter(pelicula=pelicula, reply=None, approved_comment=True).order_by('created_date')
    if request.method == 'POST':
        comment_form =CommentForm(request.POST or None)
        if comment_form.is_valid():
            text = request.POST.get('text')
            reply_id = request.POST.get('reply_id')
            comment_qs = None
            if reply_id:
                comment_qs = Comment.objects.get(id=reply_id)
            comment = Comment.objects.create(pelicula=pelicula, usuario=request.user, text=text, reply=comment_qs)
            comment.save()

            return redirect('detalle', id=pelicula.id)
    else:
        comment_form = CommentForm()

    context = {'pelicula': pelicula,
               'comment_form': comment_form,
               'comments': comments,
               #'denuncia':denuncia,
               }

    if request.is_ajax():
        html = render_to_string('comments.html', context, request=request)
        return JsonResponse({'form': html})

    return render(request, 'detalle.html', context)


def Detallefunciones(request, id, idCine):
    pelicula = Pelicula.objects.get(id=id)
    cine = Cine.objects.get(id=idCine)
    #denuncia = Comment.objects.filter(id__in=Denuncia.objects.filter(usuario=request.user).values("comment_id"))

    comments = Comment.objects.filter(pelicula=pelicula, reply=None, approved_comment=True).order_by('created_date')
    if request.method == 'POST':
        comment_form = CommentForm(request.POST or None)
        if comment_form.is_valid():
            text = request.POST.get('text')
            reply_id = request.POST.get('reply_id')
            comment_qs = None
            if reply_id:
                comment_qs = Comment.objects.get(id=reply_id)
            comment = Comment.objects.create(pelicula=pelicula, usuario=request.user, text=text, reply=comment_qs)
            comment.save()

            return redirect('detallefunciones', id=pelicula.id, idCine=cine.id)
    else:
        comment_form = CommentForm()

    is_liked = False
    if request.user.is_authenticated:
        if Megusta.objects.filter(pelicula=pelicula, usuario=request.user).exists():
            is_liked = True

    cantidad = Megusta.objects.filter(pelicula=pelicula).count()

    context = {'pelicula': pelicula,
               'cine': cine,
               'is_liked': is_liked,
               'total_likes': cantidad,
               'comment_form': comment_form,
               'comments': comments,
               #'denuncia': denuncia,
               }

    if request.is_ajax():
        html = render_to_string('comments.html', context, request=request)
        return JsonResponse({'form': html})

    return render(request, 'detallefunciones.html', context)


def Like(request, id, idCine):
    pelicula = Pelicula.objects.get(id=request.POST.get('funcion_id'))
    is_liked = False
    if Megusta.objects.filter(pelicula=pelicula, usuario=request.user):
        Megusta.objects.filter(pelicula=pelicula, usuario=request.user.id).delete()
        is_liked = False
    else:
        form = Megusta.objects.create(pelicula=pelicula, usuario=request.user)
        form.save()
        is_liked = True
    return redirect('detallefunciones', id=pelicula.id, idCine=idCine)


def Detalleestrenos(request, id, idCine):
    funcion = Funcion.objects.get(id=id)
    cine = Cine.objects.get(id=idCine)
    #denuncia = Comment.objects.filter(id__in=Denuncia.objects.filter(usuario=request.user).values("comment_id"))

    comments = Comment.objects.filter(funcion=funcion, reply=None, approved_comment=True).order_by('created_date')
    if request.method == 'POST':
        comment_form = CommentForm(request.POST or None)
        if comment_form.is_valid():
            text = request.POST.get('text')
            reply_id = request.POST.get('reply_id')
            comment_qs = None
            if reply_id:
                comment_qs = Comment.objects.get(id=reply_id)
            comment = Comment.objects.create(funcion=funcion, usuario=request.user, text=text, reply=comment_qs)
            comment.save()
            return redirect('detalleestrenos', id=funcion.id, idCine=idCine)
    else:
        comment_form = CommentForm()

    is_voto = False
    if funcion.votos.filter(id=request.user.id).exists():
        is_voto = True

    context = {'funcion': funcion,
               'cine': cine,
               'is_voto': is_voto,
               'total_votos': funcion.total_votos(),
               'comment_form': comment_form,
               'comments': comments,
               #'denuncia': denuncia,
               }

    if request.is_ajax():
        html = render_to_string('comments.html', context, request=request)
        return JsonResponse({'form': html})

    return render(request, 'detalleestrenos.html', context)


def Votar(request, id, idCine):
    funcion = Funcion.objects.get(id=request.POST.get('fun_id'))
    is_voto = False
    if funcion.votos.filter(id=request.user.id).exists():
        funcion.votos.remove(request.user)
        is_voto = False
    else:
        funcion.votos.add(request.user)
        is_voto = True
    return redirect('detalleestrenos', id=funcion.id, idCine=idCine)


@login_required
def Denuncias(request, id):
    comment = Comment.objects.get(id=id)
    if Denuncia.objects.filter(comment=comment, usuario=request.user):
        return redirect('home')
    else:
        if request.method == 'POST':
            denuncia_form = DenunciaForm(request.POST)
            if denuncia_form.is_valid():
                nuevoDenuncia = denuncia_form.save(commit=False)
                nuevoDenuncia.usuario = request.user
                nuevoDenuncia.comment = comment
                comment.cantidad_denuncia = Denuncia.objects.filter(comment=comment).count() + 1
                comment.save()
                nuevoDenuncia.save()
                return redirect('home')
        else:
            denuncia_form = DenunciaForm()
        return render(request, 'denuncia.html', {'denuncia_form': denuncia_form})


@staff_member_required
def administrarDenunciaComentarios(request):
    comentarios = Comment.objects.filter(cantidad_denuncia__gte=1).order_by('-cantidad_denuncia')
    return render(request, 'administrarDenunciaComentarios.html', {'comentarios': comentarios})


@staff_member_required
def mostrardenuncias(request, id):
    denuncias = Denuncia.objects.filter(comment_id=id)
    return render(request, 'mostrardenuncias.html', {'denuncias': denuncias})


@staff_member_required
def estadocomentario(request, id):
    comentario = Comment.objects.get(id=id)

    if comentario.approved_comment == True:
        comentario.approved_comment = False
        comentario.save()
    else:
        comentario.approved_comment = True
        comentario.save()

    return redirect('administrarDenunciaComentarios')