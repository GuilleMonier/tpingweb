from django.urls import path, include

from Cine.views import GeneroList, IdiomaList, PeliculaList, GeneroDetail
from . import views

urlpatterns = [
    path('', views.Home, name='home'),
    path('about/', views.About, name='about'),
    path('votacionestreno/', views.VotacionEstreno, name='votacionestreno'),

    #serializer
    path('genero/', views.GeneroList.as_view(), name='genero_list'),
    path('genero/<int:pk>/', views.GeneroDetail.as_view()),
    path('idioma/', IdiomaList.as_view(), name='idioma_list'),
    path('idioma/<int:pk>/', views.IdiomaDetail.as_view()),
    path('pelicula/', PeliculaList.as_view(), name='pelicula_list'),
    path('pelicula/<int:pk>/', views.PeliculaDetail.as_view()),

    path('detalle/<int:id>', views.Detalle, name='detalle'),
    path('detallefunciones/<int:id>/<int:idCine>', views.Detallefunciones, name='detallefunciones'),
    path('detalleestrenos/<int:id>/<int:idCine>', views.Detalleestrenos, name='detalleestrenos'),
    path('like/<int:id>/<int:idCine>', views.Like, name='like'),
    path('votar/<int:id>/<int:idCine>', views.Votar, name='votar'),

    path('administrar/', views.Administrar, name='administrar'),

    path('denuncia/<int:id>', views.Denuncias, name='denuncia'),

    path('administrarDenunciaComentarios/', views.administrarDenunciaComentarios, name='administrarDenunciaComentarios'),
    path('mostrardenuncias/<int:id>', views.mostrardenuncias, name='mostrardenuncias'),
    path('estadocomentario/<int:id>', views.estadocomentario, name='estadocomentario'),


    path('administrarpelicula/', views.AdministarPelicula, name='administrarpelicula'),
    path('crearpelicula/', views.CrearPelicula, name='crearpelicula'),
    path('editarpelicula/<int:id>', views.EditarPelicula, name='editarpelicula'),
    path('eliminarpelicula/<int:id>', views.EliminarPelicula, name='eliminarpelicula'),

    path('administrargenero/', views.AdministarGenero, name='administrargenero'),
    path('creargenero/', views.CrearGenero, name='creargenero'),
    path('editargenero/<int:id>', views.EditarGenero, name='editargenero'),
    path('eliminargenero/<int:id>', views.EliminarGenero, name='eliminargenero'),

    path('administraridioma/', views.AdministarIdioma, name='administraridioma'),
    path('crearidioma/', views.CrearIdioma, name='crearidioma'),
    path('editaridioma/<int:id>', views.EditarIdioma, name='editaridioma'),
    path('eliminaridioma/<int:id>', views.EliminarIdioma, name='eliminaridioma'),

    path('administrarcine/', views.AdministarCine, name='administrarcine'),
    path('crearcine/', views.CrearCine, name='crearcine'),
    path('editarcine/<int:id>', views.EditarCine, name='editarcine'),
    path('eliminarcine/<int:id>', views.EliminarCine, name='eliminarcine'),

    path('administrarfuncion/', views.AdministarFuncion, name='administrarfuncion'),
    path('crearfuncion/', views.CrearFuncion, name='crearfuncion'),
    path('editarfuncion/<int:id>', views.EditarFuncion, name='editarfuncion'),
    path('eliminarfuncion/<int:id>', views.EliminarFuncion, name='eliminarfuncion'),

    path('mostrarcine/<int:id>', views.MostrarCine, name='mostrarcine'),
    path('mostrarfunciones/<int:id>', views.MostrarFunciones, name='mostrarfunciones'),
    path('mostrarpeliculas/<int:id>', views.MostrarPeliculas, name='mostrarpeliculas'),

]

